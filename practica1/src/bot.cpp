#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "SharedMemoryData.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	//Se crea el fichero, el puntero para la proyeccióny el puntero a la memoria compartida
	int fich1;
	SharedMemoryData* pMemSh;
	char* proyeccion;

	//Se abre el fichero en modo lectura y escritura
	fich1=open("/tmp/datosbot.txt", O_RDWR);
		
	//Se proyecta el fichero
	proyeccion = (char*)mmap(NULL, sizeof(*(pMemSh)), PROT_WRITE|PROT_READ, MAP_SHARED, fich1, 0);

	//Se cierra el fichero
	close(fich1);

	//El puntero de datos va ala proyecion del fichero en memoria
	pMemSh=(SharedMemoryData*)proyeccion;

//Condición de salida
	int salir=0;

	//Acciones de la raqueta
	while (salir==0)
	{
	if (pMemSh->act==5) salir=1;

		float posRaqueta;

		//float posRaqueta2;

		posRaqueta=((pMemSh->raqueta1.y2+pMemSh->raqueta1.y1)/2);
		//posRaqueta2=((pMemSh->raqueta2.y2+pMemSh->raqueta2.y1)/2);
		if(posRaqueta < pMemSh ->esfera.centro.y)
			pMemSh->act=1;
		else if(posRaqueta> pMemSh ->esfera.centro.y)
			pMemSh->act=-1;
		else
			pMemSh->act=0;

		/*if(posRaqueta2 < pMemSh ->esfera.centro.y)
			pMemSh->act2=1;
		else if(posRaqueta2 > pMemSh ->esfera.centro.y)
			pMemSh->act2=-1;
		else
			pMemSh->act2=0;*/

		usleep(25000);
	}
	//Se desmonta la proyeccion de memoria
	munmap(proyeccion, sizeof(*(pMemSh)));
}
