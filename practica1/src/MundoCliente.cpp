// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//Cabecera modificada por Antonio Gallego 51721 

#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#include <iostream>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* proyeccion;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
/*
//Finalización de la tubería cuando acaba el tenis

	char fin[250];
	sprintf(fin, "*****TENIS FINALIZADO*****");
	write(fifo, fin, strlen(fin)+1);
	close (fifo);
*/

//Finalizacion de la proyeccion de memoria
	
	munmap(proyeccion, sizeof(ShMem));
/*

//Cierre del bot

	pMemSh->act=5;

//Tubería cliente-servidor
	close(fifocs);
	unlink("tmp/csfifo");
	close(fifoteclas);
	unlink("tmp/teclasfifo");
*/

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	count--;

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera) || jugador2.Rebota(esfera))
{
if (esfera.radio>=0.15f)
esfera.radio-=0.05f;
}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
/*
	//Mensaje a través del fifo, Jugador 2 marca un tanto
		char buf [250];
		sprintf (buf, "El Jugador 2 marca un punto, lleva %d", puntos2);
		write (fifo, buf, strlen(buf)+1);
*/
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
/*
	//Mensaje a través del fifo, Jugador 1 marca un tanto
		char buf3 [250];
		sprintf (buf3, "El Jugador 1 marca un punto, lleva %d", puntos1);
		write (fifo, buf3, strlen(buf3)+1);
*/
	}


//Actualizacion de los valores reales

	pMemSh->esfera=esfera;
	pMemSh->raqueta1=jugador1;
	pMemSh->raqueta2=jugador2;


//Implementacion para el bot del jugador1

	switch(pMemSh->act)
	{
		case 1: OnKeyboardDown('w',0,0);
			break;
		case 0: break;
		case -1: OnKeyboardDown('s',0,0);
			break;
	}
/*
//Implementacion para el bot del jugador2 tras x tiempo

	if(count<0)
	{
		switch(pMemSh->act2)
			{
				case 1: jugador2.velocidad.y=3;
					break;
				case 0: break;
				case -1: jugador2.velocidad.y=-3;
					break;
			}
	}
	

*/

//Comunicación cliente-servidor

	char cadcs[250];
	//read(fifocs,cadcs,sizeof(cadcs)); Esto no se requiere para la práctica 5

	socketcomunication.Receive(cadcs, sizeof (cadcs)); // Práctica 5

	sscanf(cadcs, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4; break;
	case 'w':jugador1.velocidad.y=4; break;
	case 'l':jugador2.velocidad.y=-4; count=250; break;
	case 'o':jugador2.velocidad.y=4; count=250; break;

/*
	case 's':
{
jugador1.y1=jugador1.y1 - 0.2;
jugador1.y2=jugador1.y2 - 0.2;
}
	case 'w':
{
jugador1.y1=jugador1.y1 +0.2;
jugador1.y2=jugador1.y2 +0.2;
}
	case 'l':
{
jugador2.y1=jugador2.y1 - 0.2;
jugador2.y2=jugador2.y2 - 0.2;
}
	case 'o':
{
jugador2.y1=jugador2.y1 +0.2;
jugador2.y2=jugador2.y2 +0.2;
}
*/
	}
	char tecla[5];
	sprintf(tecla,"%c", key);
	//write(fifoteclas,tecla,sizeof (tecla)+1); //No se requiere para la práctica 5

//Comunicación a través del Socket

	socketcomunication.Send(tecla, sizeof(tecla));
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;


	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
/*
	//Parte WRITE del logger que lleva la información
	fifo=open("/tmp/myfifologger", O_WRONLY);
*/

	//Fichero proyectado en memoria
	int fich1=open("/tmp/datosbot.txt", O_RDWR | O_CREAT | O_TRUNC, 0777); //Se crea el fichero
	write (fich1, &ShMem, sizeof(ShMem));  //Se escribe en el fichero
	proyeccion=(char*)mmap(NULL, sizeof(ShMem), PROT_WRITE|PROT_READ, MAP_SHARED, fich1, 0);  //Se asigna al puntero de proyeccion donde esta proyectado el fichero
	close (fich1); //Se cierra el fichero
	pMemSh=(SharedMemoryData*)proyeccion; //SE asigna el valor del puntero de proyeccion al puntero de datos
	pMemSh->act=0; //No se quiere que haya ninguna accion de partida

/*
//Cliente-servidor

	mkfifo("/tmp/csfifo", 0777);
	fifocs = open("/tmp/csfifo", O_RDONLY);
	mkfifo("/tmp/teclasfifo", 0777);
	fifoteclas = open("/tmp/teclasfifo", O_WRONLY);
*/

//Código práctica 5

	char nombre_cliente[100];
	printf("Introduzca el nombre del cliente: ");
	gets (nombre_cliente);

	socketcomunication.Connect((char*)"127.0.0.1", 4000);
	socketcomunication.Send(nombre_cliente, sizeof(nombre_cliente)); 
}

