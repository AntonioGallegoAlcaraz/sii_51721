#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


int main(int argc, char* argv[]){

	mkfifo("/tmp/myfifologger", 0777);
	int fd = open ("/tmp/myfifologger", O_RDONLY);
	int exit =0;
	int aux;
	while (exit == 0)
	{
		char buf2[250];
		aux = read(fd, buf2, sizeof(buf2));
		printf("%s\n" , buf2);
		if ((buf2[0]=='*')||(aux==-1)) //Con esto conseguimos que no haya error en el read
		{
			printf("*****TENIS FINALIZADO. CIERRE DEL LOGGER*****\n");
			exit = 1;
		}
	}
	
	close(fd);
	unlink("/tmp/myfifologger");
	return 0;
}
