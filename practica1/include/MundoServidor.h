// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

// Inludes para el uso de tuberías, práctica 3

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

//Includes for Shared Memory

#include "SharedMemoryData.h"
#include <sys/mman.h>

//Includes para el cliente-servidor

#include <pthread.h>

//Include para los Sockets

#include <Socket.h>

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	void RecibeComandosJugador(); //Función comandos para las teclas cliente-servidor

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int fifo;

	SharedMemoryData ShMem;
	SharedMemoryData* pMemSh;

	float count=250;
/*
	int fifocs; //Tubería cliente-servidor

	int fifoteclas; //Tubería teclas cliente-servidor
*/
	pthread_t thid1; //Thread cliente-servidor

//Sockets de la práctica 5

	Socket socketconnection;
	Socket socketcomunication;


};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
