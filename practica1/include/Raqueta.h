// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "Plano.h"
#include "Vector2D.h"
#include "glut.h"


class Raqueta : public Plano  
{
public:
	Vector2D velocidad;



	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);

	
};
